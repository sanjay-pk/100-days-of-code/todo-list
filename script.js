const itemInput = document.getElementById("inputItem")
const btnAdd = document.getElementById("addBtn")
const divUl = document.getElementById("divUl")
const ulType = document.getElementById("ulStyle")





btnAdd.addEventListener('click',()=>{
    const item = itemInput.value

if(!item){
    alert("Please Enter Something")
    li.remove()
}

// created li Element

    const li = document.createElement("li")
    li.classList.add("liStyle")
    li.innerHTML = item

// created span element

    const span = document.createElement("span")

// delete function

    const deleteBtn = document.createElement("i")
    deleteBtn.classList.add("fas", "fa-trash-alt","delete-icon")
    span.appendChild(deleteBtn)

    deleteBtn.addEventListener("click", ()=>{
        li.classList.add('removing');
        
        li.addEventListener('animationend', () => {
            li.remove();
        });
    })

    // Edit function
    const editBtn = document.createElement("i");
    editBtn.classList.add("fas", "fa-edit", "editIcon", "edit-icon");
    span.appendChild(editBtn);

    editBtn.addEventListener('click', () => {

        // Replace the li text with an input field for editing

        const inputField = document.createElement("input");
        inputField.type = "text";
        inputField.value = li.innerText;

        // Save the edited text when pressing Enter or blurring out of the input field
        
        inputField.addEventListener('blur', saveEditedText);
        inputField.addEventListener('keydown', (event) => {
            if (event.key === 'Enter') {
                saveEditedText();
            }

        });

        

        li.innerHTML = ''; // Clear existing content of the li
        
        li.appendChild(inputField); // Append the input field


        function saveEditedText() {
            const newText = inputField.value;
            li.innerText = newText;
            li.appendChild(span); // Re-append the span after saving the edited text
            if (inputField.value.length === 0) {
                li.remove()
            }
        }
    });




    // append li span ul and div

    li.appendChild(span)
    ulType.appendChild(li)
    divUl.appendChild(ulType)
    itemInput.value = "";

})